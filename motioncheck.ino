int Status = 5;  // Digital pin D1
int buzz=4; //Digital pin D2
int sensor = 13;  // Digital pin D7

void setup() {
  Serial.begin(115200);
  pinMode(sensor, INPUT);   // declare sensor as input
  pinMode(Status, OUTPUT);  // declare LED as output
  pinMode(buzz, OUTPUT); //declare BUZZER as OUTPUT
}

void loop() {

  long state = digitalRead(sensor);
    if(state == HIGH) {
      digitalWrite (Status, HIGH);
      digitalWrite (buzz, HIGH);
      Serial.println("Motion detected!");
      delay(1000);
    }
    else {
      digitalWrite (Status, LOW);
      digitalWrite (buzz, LOW);
      Serial.println("Motion absent!");
      delay(1000);
      }
}

